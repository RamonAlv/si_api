'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LoginsSchema extends Schema {
  up () {
    this.create('logins', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.string('username', 80).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('logins')
  }
}

module.exports = LoginsSchema
